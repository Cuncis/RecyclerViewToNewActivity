package com.reksa.karang.recyclerviewtonewactivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class GalleryActivity extends AppCompatActivity {

    private static final String TAG = "GalleryActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        Log.d(TAG, "onCreate: Started.");

        getIncomingIntent();
    }

    private void getIncomingIntent() {
        Log.d(TAG, "getIncomingIntent: checking for incoming intents.");

        if (getIntent().hasExtra("img_url") && getIntent().hasExtra("img_name")) {
            Log.d(TAG, "getIncomingIntent: found intent extras.");

            String imgUrl = getIntent().getStringExtra("img_url");
            String imgName = getIntent().getStringExtra("img_name");

            setImage(imgUrl, imgName);
        }
    }

    private void setImage(String imgUrl, String imgName) {
        Log.d(TAG, "setImage: setting to image and name to widgets.");

        TextView name = findViewById(R.id.img_description);
        name.setText(imgName);

        ImageView image = findViewById(R.id.img);
        Glide.with(this)
                .asBitmap()
                .load(imgUrl)
                .into(image);

    }
}
