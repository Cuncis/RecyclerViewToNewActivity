package com.reksa.karang.recyclerviewtonewactivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private ArrayList<String> mImgNames = new ArrayList<>();
    private ArrayList<String> mImgUrls = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: started.");

        initImageBitmaps();
        
    }

    private void initImageBitmaps() {
        Log.d(TAG, "initImageBitmaps: Preparing bitmap");

        mImgUrls.add("https://c1.staticflickr.com/5/4636/25316407448_de5fbf183d_o.jpg");
        mImgNames.add("Havasu Falls");

        mImgUrls.add("https://i.redd.it/tpsnoz5bzo501.jpg");
        mImgNames.add("Trondheim");

        mImgUrls.add("https://i.redd.it/qn7f9oqu7o501.jpg");
        mImgNames.add("Portugal");

        mImgUrls.add("https://i.redd.it/j6myfqglup501.jpg");
        mImgNames.add("Rocky Mountain National Park");


        mImgUrls.add("https://i.redd.it/0h2gm1ix6p501.jpg");
        mImgNames.add("Mahahual");

        mImgUrls.add("https://i.redd.it/k98uzl68eh501.jpg");
        mImgNames.add("Frozen Lake");


        mImgUrls.add("https://i.redd.it/glin0nwndo501.jpg");
        mImgNames.add("White Sands Desert");

        mImgUrls.add("https://i.redd.it/obx4zydshg601.jpg");
        mImgNames.add("Austrailia");

        mImgUrls.add("https://i.imgur.com/ZcLLrkY.jpg");
        mImgNames.add("Washington");

        initRecyclerView();
    }

    private void initRecyclerView() {
        Log.d(TAG, "initRecyclerView: init recyclerview");

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(mImgNames, mImgUrls, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
